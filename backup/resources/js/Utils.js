const Utils = window.Utils = {
	isInt: function (num) {
		if (num % 1 === 0) {
			return true
		} else if (num % 1 !== 0) {
			return false
		}
	},

	extend: function (a, b) {
		for (var i in b) {
			a[i] = b[i]
		}
	},

	hashString: function (str) {
		let hash = 0, i, chr
		if (str.length === 0) return hash
		for (i = 0; i < str.length; i++) {
			chr   = str.charCodeAt(i)
			hash  = ((hash << 5) - hash) + chr
			hash |= 0 // Convert to 32bit integer
		}
		return hash
	},

	getQueryParam: function (param) {
		let query = window.location.search.substring(1)
		let vars = query.split('&')

		for (let i = 0; i < vars.length; i++) {
			let pair = vars[i].split('=')

			if (decodeURIComponent(pair[0]) == param) {
				return decodeURIComponent(pair[1])
			}
		}
	},
}