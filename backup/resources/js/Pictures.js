const Pictures = window.Pictures = function () {
	console.log('log > Pictures - initialized!')
}

const Animations = Pictures.prototype.Animations = function () {
	console.log('log > Pictures > Animations - initialized!')

	this.pictures = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,]

	this.indexes = {
		total: null,
		prev: null,
		current: null,
		next: null
	}

	this.states = ['hidden-prev', 'prev', 'current', 'next', 'hidden-next']
	
	this.loops = {}

	this.timeline = null

	this.mouse = {
		x: 0,
		y: 0,
	}

	this.Event = new window.Event()
	this.Event.addHandler('handleMouseMovement', this.handleMouseMovement, this)
	this.Event.addHandler('handleLeftArrowClick', this.handleLeftArrowClick, this)
	this.Event.addHandler('handleRightArrowClick', this.handleRightArrowClick, this)
	this.Event.addHandler('handleNavIconTextMouseOver', this.handleNavIconTextMouseOver, this)
	this.Event.addHandler('handleNavIconTextMouseLeave', this.handleNavIconTextMouseLeave, this)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	window.addEventListener('resize', this.render.bind(this))
}

Animations.prototype.initTimeline = function () {
	this.timeline = new TimelineMax({ repeat: 0 })
	return this
}

Animations.prototype.matchUsernames = function (str) {
	return str.replace(/\B@\w*[a-zA-Z][^\s]+\w/g, function (match) {
		return `<a href="https://www.instagram.com/${match.substr(1)}/"><b>${match}</b></a>`
	})
}

Animations.prototype.matchHashtags = function (str) {
	return str.replace(/\B#\w*[a-zA-Z]+\w*/g, function (match) {
		return `<a href="https://www.instagram.com/explore/tags/${match.substr(1)}/"><b>${match}</b></a>`
	})
}

Animations.prototype.fetchFromInstagram = function (url) {
	console.log('log > Pictures > Animations - fetching from instagram')

	return new Promise(function (resolve, reject) {
		$.ajax({
			type: 'get',
			dataType: 'jsonp',
			crossOrigin: true,

			url: url,

			success: resolve,
			error: reject
		})
	})
}

Animations.prototype.addPicture = function (obj) {
	this.pictures.push(obj)
}

Animations.prototype.changePicture = function (next) {
	const pictures = document.querySelectorAll('.dyn-slider .picture')
	let timeline = new TimelineMax({ repeat: 0 })

	//console.log(this.indexes)
	
	// changing to the next picture
	if (next) {
		// isn't the last picture
		if (this.indexes.current !== 0) {
	 		// prev
			if (this.indexes.current > 0) {
				timeline.to(pictures[this.indexes.prev], 0.240, {
					x: '-100%',
					z: '-150%',
					ease: Power1.ease,
				})
				timeline.to(pictures[this.indexes.prev], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}

			// stores the current time
			timeline.add('showCurrentPicture')

	 		// current
			timeline.to(pictures[this.indexes.current], 0.240, {
				x: '0%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.current], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// next
			if (this.indexes.current < this.indexes.total - 1) {
				timeline.to(pictures[this.indexes.next], 0.240, {
					x: '100%',
					z: '-150%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.next], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}
		// is the last picture
		else {
			// animates the last picture
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				x: '-100%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

			// stores the current time
			timeline.add('showCurrentPicture')

			// brings the first picture back
			timeline.to(pictures[this.indexes.current], 0.240, {
				x: '0%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.current], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// next
			if (this.indexes.next !== null) {
				timeline.to(pictures[this.indexes.next], 0.240, {
					x: '100%',
					z: '-150%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.next], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}
	}
	// changing to the previous picture
	else {
		// isn't the last picture
		if (this.indexes.current !== this.indexes.total - 1) {

	 		// next
			if (this.indexes.current < this.indexes.total - 1) {
				timeline.to(pictures[this.indexes.next], 0.240, {
					x: '100%',
					z: '-150%',
					ease: Power1.ease,
				})
				timeline.to(pictures[this.indexes.next], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}

			// stores the current time
			timeline.add('showCurrentPicture')

	 		// current
			timeline.to(pictures[this.indexes.current], 0.240, {
				x: '0%',
				z: '-100%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.current], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// prev
			if (this.indexes.current > 0) {
				timeline.to(pictures[this.indexes.prev], 0.240, {
					x: '-100%',
					z: '-100%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.prev], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}

		// is the last picture
		else {
			// animates the last picture
			timeline.to(pictures[0], 0.240, {
				x: '-100%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[0], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

			// stores the current time
			timeline.add('showCurrentPicture')

			// brings the first picture back
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				x: '0%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// prev
			if (this.indexes.current > 0) {
				timeline.to(pictures[this.indexes.prev], 0.240, {
					x: '-100%',
					z: '-100%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.prev], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}
	}

	;[].forEach.call(pictures, function (el, i) {
		if (
			i !== this.indexes.prev &&
			i !== this.indexes.current &&
			i !== this.indexes.next
		) {
			if (i < this.indexes.prev) {
				timeline.set(el, {
					x: '-200%',
					ease: Power1.ease,
				})
			}
			if (i > this.indexes.next) {
				timeline.set(el, {
					x: '200%',
					ease: Power1.ease,
				})
			}
		}
	}, this)

	return this
}

Animations.prototype.clearState = function (index) {
	let pictures = document.querySelectorAll('.picture')

	TweenMax.set(pictures[index], {
		x: '0%',
		z: '0%',
		y: '0%',
	})

	return this
}

Animations.prototype.setIndexes = function () {
	this.pictures = this.pictures.reverse()
	this.indexes.total = this.pictures.length

	if (this.indexes.prev === null && this.indexes.current === null && this.indexes.next === null) {
		
		if (this.indexes.total > 2) {
			this.indexes.current = Math.floor(this.indexes.total / 2)
		} else {
			this.indexes.current = 0
		}
		
		if (this.indexes.current > 0) {
			this.indexes.prev = this.indexes.current - 1
		} else {
			this.indexes.prev = null
		}

		if (this.indexes.current < this.indexes.total - 1) {
			this.indexes.next = this.indexes.current + 1
		} else {
			this.indexes.next = null
		}
	}

	return this
}

Animations.prototype.updateIndexes = function (index, callback) {
	this.indexes.current = index

	if (this.indexes.current > 0) {
		this.indexes.prev = this.indexes.current - 1
	} else {
		this.indexes.prev = null
	}

	if (this.indexes.current < this.indexes.total - 1) {
		this.indexes.next = this.indexes.current + 1
	} else {
		this.indexes.next = null
	}

	if (callback) callback()

	return this
}

Animations.prototype.mouseRotationLoop = function () {
	let container = document.querySelector('body')
	let pictureWrapper = document.querySelectorAll('.picture-wrapper')

	let cx = container.offsetWidth / 2
	let cy = container.offsetHeight / 2

	let dx = this.mouse.x - cx
	let dy = this.mouse.y - cy

	let tiltx = (dy / cy)
	let tilty = -(dx / cx)

	let radius = Math.sqrt(Math.pow(tiltx, 2) + Math.pow(tilty, 2))
	let degree = (radius * 20)
	let transform = 'rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg) scale(0.85)'

	;[].forEach.call(pictureWrapper, (el, i) => {
		TweenMax.to(el, 1, {
			transform: transform,
			ease: Power2.easeOut
		})
	})

	TweenMax.to('.picture-arrows', 1, {
		transform: transform,
		ease: Power2.easeOut
	})
}

Animations.prototype.handleMouseMovement = function (event) {
	this.mouse.x = event.pageX
	this.mouse.y = event.pageY

	if (this.loops.mouseRotation) cancelAnimationFrame(this.loops.mouseRotation)
	this.loops.mouseRotation = requestAnimationFrame(this.mouseRotationLoop.bind(this))
}

Animations.prototype.handleLeftArrowClick = function (event) {
	if (this.indexes.prev !== null) {
		this.updateIndexes(this.indexes.prev)
	} else {
		this.updateIndexes(this.indexes.total - 1)
	}
	
	this.changePicture()

	return this
}

Animations.prototype.handleRightArrowClick = function (event) {
	if (this.indexes.next !== null) {
		this.updateIndexes(this.indexes.next)
	} else {
		this.updateIndexes(0)
	}

	this.changePicture(1)

	return this
}

Animations.prototype.handleNavIconTextMouseOver = function (event) {
	let parent = event.target.parentNode.parentNode
	let text = parent.querySelector('.nav-icon-text')
	let icon = text.parentNode.querySelector('svg')

	TweenMax.to(icon, 0.240, {
		webkitClipPath: 'inset(0px 0px 0px 100%)',
		ease: Back.easeOut,
	})

	TweenMax.set(text, {
		display: 'block',
		ease: Back.easeOut,
	})

	TweenMax.to(text, 1, {
		x: '0px',
		ease: Back.easeOut,
	})

	return this
}

Animations.prototype.handleNavIconTextMouseLeave = function (event) {
	console.log('handleNavIconTextMouseLeave')
	let parent = event.target.parentNode.parentNode
	let text = parent.querySelector('.nav-icon-text')
	let icon = text.parentNode.querySelector('svg')

	TweenMax.set(text, {
		display: 'none'
	})

	TweenMax.to(text, 0.240, {
		x: '200px',
		ease: Back.easeOut,
	})

	TweenMax.to(icon, 0.240, {
		webkitClipPath: 'inset(0px 0px 0px 0%)',
		ease: Back.easeOut,
	})

	return this
}

Animations.prototype.loadPictures = function () {
	console.log('log > Pictures > Animations - loading pictures')

	const url = 'https://api.instagram.com/v1/users/2113546503/media/recent?access_token=2113546503.5971227.62fea53d2d12432eafcd6c8505dc6e4d&callback=callback'
	//const url = 'https://api.instagram.com/v1/users/4582344969/media/recent?access_token=4840249548.d681e8b.517b0e4c5e514ecb84ba701da0eb23eb&callback=callback'

	this.fetchFromInstagram(url)
		.then((payload) => {
			if (payload.data.length) {
				payload.data.forEach((el, ind, arr) => {
					if (ind < 10) {
						this.addPicture(el)
					}
				})
			}
		})
		.then(() => {
			this.setIndexes().renderPictures()
		})
}

Animations.prototype.renderPictures = function () {
	console.log('log > Pictures > Animations - rendering pictures')

	this.pictures.forEach((el, i) => {
		let pictures = document.querySelector('.pictures')
		let pictureArrows = document.querySelector('.picture-arrows')

		let picture = document.createElement('div')
		picture.classList.add('picture')

		if (i < this.indexes.prev) {
			TweenMax.set(picture, {
				x: '-200%',
			})
		}

		if (i === this.indexes.prev) {
			TweenMax.set(picture, {
				x: '-100%',
			})
		}

		if (i === this.indexes.current) {
			TweenMax.set(picture, {
				x: '0',
			})
		}

		if (i === this.indexes.next) {
			TweenMax.set(picture, {
				x: '100%',
			})
		}

		if (i > this.indexes.next) {
			TweenMax.set(picture, {
				x: '200%',
			})
		}

		let pictureWrapper = document.createElement('div')
		pictureWrapper.classList.add('picture-wrapper')

		let img = document.createElement('img')
		img.src = el.images.standard_resolution.url

		let pictureDescription = document.createElement('p')
		pictureDescription.classList.add('picture-description')
		let description = this.matchHashtags(el.caption.text)
		description = this.matchUsernames(description)
		pictureDescription.innerHTML = description

		pictureWrapper.appendChild(img)
		pictureWrapper.appendChild(pictureDescription)
		picture.appendChild(pictureWrapper)

		pictures.insertBefore(picture, pictureArrows)

		//console.log(el)
	})

	return this
}

Animations.prototype.renderAnimations = function () {
	console.log('log > Pictures > Animations - rendering main animations')

	// Show the first white logo
	this.timeline.fromTo(".splash-logo-white svg", 2, {
		clip: 'rect(0,0,100%,0)',
		ease: SlowMo.ease.config(0.1, 0, false),
	}, {
		clip: 'rect(0,370px,100%,0)',
		ease: Power4.easeOut,
	})

	// Put the black logo on the top of the white logo
	// creating a fill effect.
	this.timeline.set(".splash-logo-black svg", {
		visibility: 'visible',
		clip: 'rect(0,0,100%,0)',
	})
	this.timeline.to(".splash-logo-black svg", 2, {
		clip: 'rect(0,370px,100%,0)',
		//ease: SlowMo.ease.config(0.7, 0.7, false),
		ease: Power4.easeOut,
	})

	// Set a label when the logo finishes loading
	this.timeline.add('logoFinishedLoading')

	// Create a reveal effect with a black overlay
	this.timeline.to(".overlay", 0.8, {
		x: 0,
		ease: Power4.easeIn,
	}, "logoFinishedLoading")
	this.timeline.to(".overlay", 0.6, {
		x: "100%",
		ease: Power4.easeOut,
	}, "logoFinishedLoading+=0.6")

	// Clips both logos on reveal
	this.timeline.to(".splash-logo-black svg", 0.2, {
		clip: 'rect(0,370px,100%,370px)',
		//ease: SlowMo.ease.config(0.7, 0.7, false),
		ease: Power4.easeOut,
	}, "logoFinishedLoading+=0.6")
	this.timeline.to(".splash-logo-white svg", 0.2, {
		clip: 'rect(0,370px,100%,370px)',
		//ease: SlowMo.ease.config(0.7, 0.7, false),
		ease: Power4.easeOut,
	}, "logoFinishedLoading+=0.6")

	// Reveal the application main div
	this.timeline.to(".main", 0.8, {
		scale: 1,
		x: 0,
		//ease: SlowMo.ease.config(0.7, 0.7, false),
		ease: Power4.easeOut,
	}, "logoFinishedLoading+=0.8")

	/*
	this.timeline.set('.splash-screen', {
		backgroundColor: '#000',
		ease: Power1.ease,
	})

	this.timeline.to(".main", 0.8, {
		scale: 0.9,
		ease: Power4.easeOut,
	})
	*/

	// Set a label when the application logo animation starts 
	this.timeline.add('appLogoLoading')

	// Reveal the application logo
	this.timeline.to(".nav-logo", 1, {
		webkitClipPath: 'inset(0px 0% 0px 0px)',
		ease: Back.easeOut,
	})

	/*
	// Reveal the navigation icons
	this.timeline.to(".nav-icon", 0.75, {
		webkitClipPath: 'inset(0px 0% 0px 0px)',
		ease: Back.easeOut,
	}, 'appLogoLoading+=0.25')
	*/

	// Reveal the application main section
	this.timeline.to(".dyn-slider", 1, {
		webkitClipPath: 'inset(0% 0% 0px 0px)',
		ease: Back.easeOut,
	}, 'appLogoLoading+=0.25')

	// Set a label when the logo finishes loading
	this.timeline.add('appRevealFinished')

	// Text box lines detail
	this.timeline.to(CSSRulePlugin.getRule('.picture-wrapper .picture-description:before'), 1, {
		cssRule: { width: '85%' },
		ease: Back.easeOut,
	}, 'appRevealFinished')
	this.timeline.to(CSSRulePlugin.getRule('.picture-wrapper .picture-description:after'), 1, {
		cssRule: { height: '85%' },
		ease: Back.easeOut,
	}, 'appRevealFinished')
}

Animations.prototype.render = function (event) {
	console.log('log > Pictures > Animations - render called!')

	const mouseArea = document.querySelector('.dyn-slider')
	const arrowLeft = document.querySelector('.line-arrow.left')
	const arrowRight = document.querySelector('.line-arrow.right')
	const navIconsWithText = document.querySelectorAll('.nav-icon.with-text')

	this.Event.addTo(mouseArea, 'mousemove', 'handleMouseMovement')
	this.Event.addTo(arrowLeft, 'click', 'handleLeftArrowClick')
	this.Event.addTo(arrowRight, 'click', 'handleRightArrowClick')
	//this.Event.addTo(navIconsWithText, 'mouseover', 'handleNavIconTextMouseOver')
	//this.Event.addTo(navIconsWithText, 'mouseleave', 'handleNavIconTextMouseLeave')

	this.initTimeline()
	this.setIndexes()
	//this.loadPictures()
	//this.renderAnimations()
}

