CREATE DATABASE IF NOT EXISTS `castelodaserra` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `castelodaserra`;

CREATE TABLE IF NOT EXISTS `rise_config` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`value`				longtext 		NOT NULL,
	`autoload`			bool 			NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_users` (
	`id`				varchar(255)    NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255)	NOT NULL,
	`email`				varchar(255)	NOT NULL,
	`password`			varchar(255)	NOT NULL,
	`picture`			varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`) VALUES ('0cbdeb00', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg');

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_url', 'http://castelodaserra.com.br/', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_home', 'http://castelodaserra.com.br/', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_name', 'Castelo da Serra', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_description', '', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('theme', 'castelodaserra', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('timezone_string', 'America/Sao_Paulo', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_sender', 'Castelo da Serra', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_url', 'smtp.castelodaserra.com.br', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_login', 'contato@castelodaserra.com.br', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_pass', 'contatoserra18', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_port', '465', true);
