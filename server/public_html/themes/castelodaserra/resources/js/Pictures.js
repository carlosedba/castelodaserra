const Pictures = window.Pictures = function () {
	console.log('log > Pictures - initialized!')
}

const Animations = Pictures.prototype.Animations = function () {
	console.log('log > Pictures > Animations - initialized!')

	this.pictures = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ]

	this.indexes = {
		total: null,
		prev: null,
		current: null,
		next: null
	}

	this.states = ['hidden-prev', 'prev', 'current', 'next', 'hidden-next']
	
	this.loops = {}

	this.timeline = null

	this.mouse = {
		x: 0,
		y: 0,
	}

	this.Event = new window.XEvent()
	this.Event.addHandler('handleMouseMovement', this.handleMouseMovement, this)
	this.Event.addHandler('handleLeftArrowClick', this.handleLeftArrowClick, this)
	this.Event.addHandler('handleRightArrowClick', this.handleRightArrowClick, this)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	window.addEventListener('resize', this.render.bind(this))
}

Animations.prototype.initTimeline = function () {
	this.timeline = new TimelineMax({ repeat: 0 })
	return this
}

Animations.prototype.matchUsernames = function (str) {
	return str.replace(/\B@\w*[a-zA-Z][^\s]+\w/g, function (match) {
		return `<a href="https://www.instagram.com/${match.substr(1)}/"><b>${match}</b></a>`
	})
}

Animations.prototype.matchHashtags = function (str) {
	return str.replace(/\B#\w*[a-zA-Z]+\w*/g, function (match) {
		return `<a href="https://www.instagram.com/explore/tags/${match.substr(1)}/"><b>${match}</b></a>`
	})
}

Animations.prototype.fetchFromInstagram = function (url) {
	console.log('log > Pictures > Animations - fetching from instagram')

	return new Promise(function (resolve, reject) {
		$.ajax({
			type: 'get',
			dataType: 'jsonp',
			crossOrigin: true,

			url: url,

			success: resolve,
			error: reject
		})
	})
}

Animations.prototype.addPicture = function (obj) {
	this.pictures.push(obj)
}

Animations.prototype.changePicture = function (next) {
	const pictures = document.querySelectorAll('.dyn-slider .picture')
	let timeline = new TimelineMax({ repeat: 0 })

	console.log(this.indexes)
	console.log(pictures)
	
	// changing to the next picture
	if (next) {
		// isn't the last picture
		if (this.indexes.current !== 0) {
	 		// prev
			if (this.indexes.current > 0) {
				timeline.to(pictures[this.indexes.prev], 0.240, {
					x: '-100%',
					z: '-150%',
					ease: Power1.ease,
				})
				timeline.to(pictures[this.indexes.prev], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}

			// stores the current time
			timeline.add('showCurrentPicture')

	 		// current
			timeline.to(pictures[this.indexes.current], 0.240, {
				x: '0%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.current], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// next
			if (this.indexes.current < this.indexes.total - 1) {
				timeline.to(pictures[this.indexes.next], 0.240, {
					x: '100%',
					z: '-150%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.next], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}
		// is the last picture
		else {
			// animates the last picture
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				x: '-100%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

			// stores the current time
			timeline.add('showCurrentPicture')

			// brings the first picture back
			timeline.to(pictures[this.indexes.current], 0.240, {
				x: '0%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.current], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// next
			if (this.indexes.next !== null) {
				timeline.to(pictures[this.indexes.next], 0.240, {
					x: '100%',
					z: '-150%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.next], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}
	}
	// changing to the previous picture
	else {
		// isn't the last picture
		if (this.indexes.current !== this.indexes.total - 1) {

	 		// next
			if (this.indexes.current < this.indexes.total - 1) {
				timeline.to(pictures[this.indexes.next], 0.240, {
					x: '100%',
					z: '-150%',
					ease: Power1.ease,
				})
				timeline.to(pictures[this.indexes.next], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}

			// stores the current time
			timeline.add('showCurrentPicture')

	 		// current
			timeline.to(pictures[this.indexes.current], 0.240, {
				x: '0%',
				z: '-100%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.current], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// prev
			if (this.indexes.current > 0) {
				timeline.to(pictures[this.indexes.prev], 0.240, {
					x: '-100%',
					z: '-100%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.prev], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}

		// is the last picture
		else {
			// animates the last picture
			timeline.to(pictures[0], 0.240, {
				x: '-100%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[0], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

			// stores the current time
			timeline.add('showCurrentPicture')

			// brings the first picture back
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				x: '0%',
				z: '-150%',
				ease: Power1.ease,
			}, 'showCurrentPicture')
			timeline.to(pictures[this.indexes.total - 1], 0.240, {
				z: '0%',
				ease: Power4.ease,
			})

	 		// prev
			if (this.indexes.current > 0) {
				timeline.to(pictures[this.indexes.prev], 0.240, {
					x: '-100%',
					z: '-100%',
					ease: Power1.ease,
				}, 'showCurrentPicture')
				timeline.to(pictures[this.indexes.prev], 0.240, {
					z: '0%',
					ease: Power4.ease,
				})
			}
		}
	}

	;[].forEach.call(pictures, function (el, i) {
		if (
			i !== this.indexes.prev &&
			i !== this.indexes.current &&
			i !== this.indexes.next
		) {
			if (i < this.indexes.prev) {
				timeline.set(el, {
					x: '-200%',
					ease: Power1.ease,
				})
			}
			if (i > this.indexes.next) {
				timeline.set(el, {
					x: '200%',
					ease: Power1.ease,
				})
			}
		}
	}, this)

	return this
}

Animations.prototype.clearState = function (index) {
	let pictures = document.querySelectorAll('.dyn-slider .picture')

	TweenMax.set(pictures[index], {
		x: '0%',
		z: '0%',
		y: '0%',
	})

	return this
}

Animations.prototype.setIndexes = function () {
	this.pictures = this.pictures.reverse()
	this.indexes.total = this.pictures.length

	if (this.indexes.prev === null && this.indexes.current === null && this.indexes.next === null) {
		
		if (this.indexes.total > 2) {
			this.indexes.current = Math.floor(this.indexes.total / 2)
		} else {
			this.indexes.current = 0
		}
		
		if (this.indexes.current > 0) {
			this.indexes.prev = this.indexes.current - 1
		} else {
			this.indexes.prev = null
		}

		if (this.indexes.current < this.indexes.total - 1) {
			this.indexes.next = this.indexes.current + 1
		} else {
			this.indexes.next = null
		}
	}

	return this
}

Animations.prototype.updateIndexes = function (index, callback) {
	this.indexes.current = index

	if (this.indexes.current > 0) {
		this.indexes.prev = this.indexes.current - 1
	} else {
		this.indexes.prev = null
	}

	if (this.indexes.current < this.indexes.total - 1) {
		this.indexes.next = this.indexes.current + 1
	} else {
		this.indexes.next = null
	}

	if (callback) callback()

	return this
}

Animations.prototype.mouseRotationLoop = function () {
	let container = document.querySelector('body')
	let pictureWrapper = document.querySelectorAll('.dyn-slider .picture a')

	let cx = container.offsetWidth / 2
	let cy = container.offsetHeight / 2

	let dx = this.mouse.x - cx
	let dy = this.mouse.y - cy

	let tiltx = (dy / cy)
	let tilty = -(dx / cx)

	let radius = Math.sqrt(Math.pow(tiltx, 2) + Math.pow(tilty, 2))
	let degree = (radius * 20)
	let transform = 'rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg) scale(0.85)'

	;[].forEach.call(pictureWrapper, (el, i) => {
		TweenMax.to(el, 1, {
			transform: transform,
			ease: Power2.easeOut
		})
	})

	TweenMax.to('.picture-arrows', 1, {
		transform: transform,
		ease: Power2.easeOut
	})
}

Animations.prototype.handleMouseMovement = function (event) {
	this.mouse.x = event.pageX
	this.mouse.y = event.pageY

	if (this.loops.mouseRotation) cancelAnimationFrame(this.loops.mouseRotation)
	this.loops.mouseRotation = requestAnimationFrame(this.mouseRotationLoop.bind(this))
}

Animations.prototype.handleLeftArrowClick = function (event) {
	if (this.indexes.prev !== null) {
		this.updateIndexes(this.indexes.prev)
	} else {
		this.updateIndexes(this.indexes.total - 1)
	}
	
	this.changePicture()

	return this
}

Animations.prototype.handleRightArrowClick = function (event) {
	if (this.indexes.next !== null) {
		this.updateIndexes(this.indexes.next)
	} else {
		this.updateIndexes(0)
	}

	this.changePicture(1)

	return this
}

Animations.prototype.render = function (event) {
	console.log('log > Pictures > Animations - render called!')

	const mouseArea = document.querySelector('.dyn-slider')
	const arrowLeft = document.querySelector('.line-arrow.left')
	const arrowRight = document.querySelector('.line-arrow.right')

	this.Event.addTo(mouseArea, 'mousemove', 'handleMouseMovement')
	this.Event.addTo(arrowLeft, 'click', 'handleLeftArrowClick')
	this.Event.addTo(arrowRight, 'click', 'handleRightArrowClick')

	this.initTimeline()
	this.setIndexes()
}

