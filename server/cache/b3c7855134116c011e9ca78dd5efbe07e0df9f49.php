<?php $__env->startSection('scripts'); ?>
	<!-- Scripts -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.5.0/bluebird.core.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.2.0.min.js" integrity="sha256-JAW99MJVpJBGcbzEuXk4Az05s/XyDdBomFqNlM3ic+I=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/gsap/TweenMax.min.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/gsap/TimelineMax.min.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/gsap/plugins/CSSRulePlugin.min.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/clip-path/clip-path.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/photoswipe/photoswipe.min.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/photoswipe/photoswipe-ui-default.min.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/js/Utils.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/js/Event.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/js/Form.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/js/Pictures.js"></script>
	<script type="text/javascript" src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/js/Bootstrap.js"></script>
	<?php echo $__env->yieldSection(); ?>