<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php $__env->startSection('vendor-css'); ?>
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/normalize/normalize.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/hamburgers/hamburgers.min.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/photoswipe/photoswipe.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/photoswipe/default-skin/default-skin.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('application-css'); ?>
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/css/inputs.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/css/buttons.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/css/section.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/css/main.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('fonts'); ?>
		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/fonts/lemonmilk/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/fonts/champagnelimousines/stylesheet.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('js'); ?>
		<script src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/vendor/modernizr/modernizr-custom.js"></script>
		<?php echo $__env->yieldSection(); ?>
</head>