<?php $__env->startSection('title'); ?>
	Castelo da Serra
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
	##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
	##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
	##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

	<!-- Hero -->
	<section class="hero">
		<div class="hero-slides">
			<div class="hero-slide" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/EME_9036.jpg')"></div>
		</div>
		<div class="hero-titles">
			<p class="hero-pretitle">O charme da natureza, e um lugar especial</p>
			<p class="hero-title">O seu evento, inesquecível!</p>
		</div>
	</section>

	<section class="section section-one">
		<div class="section-wrapper">
			<div class="section-row">
				<div class="section-columns">
					<div class="section-column auto">
						<div class="section-titles">
							<p class="section-title bold">Oferecemos suporte para:</p>
							<p class="section-subtitle"></p>
							<div class="section-text">
								<ul>
									<li>Casamentos e Bodas</li>
									<li>Aniversários e Batizados</li>
									<li>Reuniões Corporativas</li>
									<li>Confraternizações</li>
									<li>Formaturas</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="section-column auto">
						<div class="picture-switcher">
							<div class="picture" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0155-min.jpg')"></div>
							<div class="picture" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0156-min.jpg')"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section section-two">
		<div class="section-wrapper">
			<div class="section-row small-pad">
				<div class="section-columns">
					<div class="section-column auto">
						<div class="section-titles">
							<p class="section-title bold">Conheça nosso local!</p>
							<p class="section-subtitle"></p>
							<div class="section-text">
								<p>Desde sua bela entrada, o Castelo da Serra lhe recebe com um belo e amplo pátio para a realização de seu evento. Nossa casa é completa e permite desde a hospedagem à utilização da cozinha para almoços e jantares.<br><br>Confira nossas fotos ao lado!</p>
							</div>
						</div>
					</div>
					<div class="section-column auto">
						<div class="dyn-slider">
							<div class="pictures">

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0157-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0157-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0157-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0041-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0041-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0041-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0038-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0038-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0038-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0029-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0029-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0029-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0149-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0149-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0149-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0141-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0141-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0141-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0140-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0140-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0140-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0139-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0139-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0139-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0125-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0125-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0125-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/EME_9021.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/EME_9021.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/EME_9021.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0119-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0119-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0119-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0109-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0109-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0109-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0106-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0106-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0106-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0102-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0102-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0102-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0088-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0088-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0088-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0087-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0087-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0087-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0086-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0086-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0086-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0076-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0076-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0076-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0074-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0074-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0074-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0059-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0059-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0059-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0044-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0044-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0044-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0006-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0006-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0006-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0009-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0009-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0009-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

								<div class="picture">
									<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
										<a href="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0021-min.jpg" itemprop="contentUrl" data-size="1024x768" style="background-image: url('http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0021-min.jpg')">
											<img src="http://localhost/castelodaserra/server/public_html//themes/castelodaserra/resources/img-min/foto-0021-min.jpg" itemprop="thumbnail" alt="Description" />
										</a>
										<figcaption itemprop="caption description"></figcaption>
									</figure>
								</div>

							</div>
							<div class="picture-arrows">
								<a class="line-arrow left"></a>
								<a class="line-arrow right"></a>
							</div>
						</div>
						<!-- Root element of PhotoSwipe. Must have class pswp. -->
						<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

						<!-- Background of PhotoSwipe. 
							It's a separate element, as animating opacity is faster than rgba(). -->
							<div class="pswp__bg"></div>

							<!-- Slides wrapper with overflow:hidden. -->
							<div class="pswp__scroll-wrap">

								<!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
								<!-- don't modify these 3 pswp__item elements, data is added later on. -->
								<div class="pswp__container">
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
								</div>

								<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
								<div class="pswp__ui pswp__ui--hidden">

									<div class="pswp__top-bar">

										<!--  Controls are self-explanatory. Order can be changed. -->

										<div class="pswp__counter"></div>

										<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

										<button class="pswp__button pswp__button--share" title="Share"></button>

										<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

										<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

										<!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
										<!-- element will get class pswp__preloader--active when preloader is running -->
										<div class="pswp__preloader">
											<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
													<div class="pswp__preloader__donut"></div>
												</div>
											</div>
										</div>
									</div>

									<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
										<div class="pswp__share-tooltip"></div> 
									</div>

									<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
									</button>

									<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
									</button>

									<div class="pswp__caption">
										<div class="pswp__caption__center"></div>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="onde-ficamos" class="section section-one">
		<div class="section-wrapper">
			<div class="section-row">
				<div class="section-columns">
					<div class="section-column">
						<div class="section-titles">
							<p class="section-title bold">Onde ficamos</p>
							<p class="section-subtitle"></p>
							<p class="section-text">
								Estamos localizados na Rua Nova Tirol, 1188<br>
								no bairro Capoeira Dos Dinos em Piraquara.<br><br>
								Atendemos pelos telefones:<br>
								(41) 3673-9988 - (41) 99255-1188
							</p>
						</div>
					</div>
					<div class="section-column"></div>
				</div>
			</div>
		</div>
		<div class="google-map"></div>
	</section>

	<section id="orcamento" class="section section-one">
		<div class="section-wrapper">
			<div class="section-row">
				<div class="section-titles">
					<p class="section-title bold">Faça um orçamento</p>
				</div>	
				<div class="form">
					<form method="POST" action="<?php echo e($config['site_url']); ?>email/contato">
						<div class="inputs">
							<div class="input input-monster">
								<label>Nome: *</label>
								<input type="text" name="nome" placeholder="" required>
							</div>
							<div class="multi-input">
								<div class="input input-big">
									<label>E-mail: *</label>
									<input type="email" name="email" placeholder="email@exemplo.com" required>
								</div>
								<div class="input input-big">
									<label>Telefone: *</label>
									<input type="tel" name="telefone" placeholder="(DDD) 00000-0000" required>
								</div>
							</div>
							<div class="multi-input">
								<div class="input input-medium">
									<label>Data do evento: *</label>
									<input type="date" name="data" placeholder="" required>
								</div>
								<div class="input input-medium">
									<label>Tipo de evento: *</label>
									<select name="tipo" required>
	                                    <option selected disabled>Selecione</option>
										<option>Casamento</option>
										<option>Bodas</option>
										<option>Aniversário</option>
										<option>Batizado</option>
										<option>Reunião corporativa</option>
										<option>Confraternização</option>
										<option>Formatura</option>
									</select>
								</div>
								<div class="input input-medium">
									<label>Número de pessoas: *</label>
									<input type="text" name="numero" placeholder="" required>
								</div>
							</div>
							<div class="input input-monster">
								<label>Mensagem:</label>
								<textarea placeholder="Sua mensagem..." name="mensagem"></textarea>
							</div>
						</div>
						<a type="submit" href="javascript:void(0)" class="btn-submit">Enviar</a>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
	##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

	<script>
      function initMap() {

        // Create a new StyledMapType object, passing it an array of styles,
        // and the name to be displayed on the map type control.
        var styledMapType = new google.maps.StyledMapType(
            [
			    {
			        "featureType": "all",
			        "elementType": "labels.text.fill",
			        "stylers": [
			            {
			                "saturation": 36
			            },
			            {
			                "color": "#333333"
			            },
			            {
			                "lightness": 40
			            }
			        ]
			    },
			    {
			        "featureType": "all",
			        "elementType": "labels.text.stroke",
			        "stylers": [
			            {
			                "visibility": "on"
			            },
			            {
			                "color": "#ffffff"
			            },
			            {
			                "lightness": 16
			            }
			        ]
			    },
			    {
			        "featureType": "all",
			        "elementType": "labels.icon",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "administrative",
			        "elementType": "geometry.fill",
			        "stylers": [
			            {
			                "color": "#fefefe"
			            },
			            {
			                "lightness": 20
			            }
			        ]
			    },
			    {
			        "featureType": "administrative",
			        "elementType": "geometry.stroke",
			        "stylers": [
			            {
			                "color": "#fefefe"
			            },
			            {
			                "lightness": 17
			            },
			            {
			                "weight": 1.2
			            }
			        ]
			    },
			    {
			        "featureType": "landscape",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "lightness": 20
			            },
			            {
			                "color": "#fef6d3"
			            }
			        ]
			    },
			    {
			        "featureType": "poi",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#f5f5f5"
			            },
			            {
			                "lightness": 21
			            }
			        ]
			    },
			    {
			        "featureType": "poi.attraction",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "poi.government",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "poi.park",
			        "elementType": "all",
			        "stylers": [
			            {
			                "visibility": "on"
			            }
			        ]
			    },
			    {
			        "featureType": "poi.park",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "lightness": 21
			            },
			            {
			                "color": "#ffefa0"
			            },
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "road.highway",
			        "elementType": "geometry.fill",
			        "stylers": [
			            {
			                "color": "#ffffff"
			            },
			            {
			                "lightness": 17
			            }
			        ]
			    },
			    {
			        "featureType": "road.highway",
			        "elementType": "geometry.stroke",
			        "stylers": [
			            {
			                "color": "#ffffff"
			            },
			            {
			                "lightness": 29
			            },
			            {
			                "weight": 0.2
			            }
			        ]
			    },
			    {
			        "featureType": "road.arterial",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#ffffff"
			            },
			            {
			                "lightness": 18
			            }
			        ]
			    },
			    {
			        "featureType": "road.local",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#ffffff"
			            },
			            {
			                "lightness": 16
			            }
			        ]
			    },
			    {
			        "featureType": "transit",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#f2f2f2"
			            },
			            {
			                "lightness": 19
			            }
			        ]
			    },
			    {
			        "featureType": "water",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "lightness": 17
			            },
			            {
			                "color": "#c8ebff"
			            }
			        ]
			    }
			],
            {name: 'Castelo da Serra'});

        // Create a map object, and include the MapTypeId to add
        // to the map type control.

		var position = {lat: -25.472326, lng: -49.0562227}

        var map = new google.maps.Map(document.querySelector('.google-map'), {
          center: {lat: position.lat + 0.0040000, lng: position.lng - 0.0600000},
          zoom: 13,
          disableDefaultUI: true,
          mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                    'styled_map']
          }
        });

        var marker = new google.maps.Marker({
          position: position,
          map: map,
          title: 'Castelo da Serra'
        })

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMKGqpxUhrjIIqAqkDOF5VzFSJKBT_q_g&callback=initMap">
    </script> 
	<script type="text/javascript">
		var initPhotoSwipeFromDOM = function(gallerySelector) {

		    // parse slide data (url, title, size ...) from DOM elements 
		    // (children of gallerySelector)
		    var parseThumbnailElements = function(el) {
		        var thumbElements = el.querySelectorAll('figure'),
		            numNodes = thumbElements.length,
		            items = [],
		            figureEl,
		            linkEl,
		            size,
		            item;

		        for(var i = 0; i < numNodes; i++) {

		            figureEl = thumbElements[i]; // <figure> element

		            // include only element nodes 
		            if(figureEl.nodeType !== 1) {
		                continue;
		            }

		            linkEl = figureEl.children[0]; // <a> element

		            size = linkEl.getAttribute('data-size').split('x');

		            // create slide object
		            item = {
		                src: linkEl.getAttribute('href'),
		                w: parseInt(size[0], 10),
		                h: parseInt(size[1], 10)
		            };



		            if(figureEl.children.length > 1) {
		                // <figcaption> content
		                item.title = figureEl.children[1].innerHTML; 
		            }

		            if(linkEl.children.length > 0) {
		                // <img> thumbnail element, retrieving thumbnail url
		                item.msrc = linkEl.children[0].getAttribute('src');
		            } 

		            item.el = figureEl; // save link to element for getThumbBoundsFn
		            items.push(item);
		        }

		        return items;
		    };

		    // find nearest parent element
		    var closest = function closest(el, fn) {
		        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
		    };

		    // triggers when user clicks on thumbnail
		    var onThumbnailsClick = function(e) {
		        e = e || window.event;
		        e.preventDefault ? e.preventDefault() : e.returnValue = false;

		        var eTarget = e.target || e.srcElement;

		        // find root element of slide
		        var clickedListItem = closest(eTarget, function(el) {
		            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
		        });

		        var clickedListPicture = closest(eTarget, function(el) {
		            return (el.classList && el.classList.contains('picture'));
		        });

		        if(!clickedListItem) {
		            return;
		        }

		        // find index of clicked item by looping through all child nodes
		        // alternatively, you may define index via data- attribute
		        var clickedGallery = clickedListPicture.parentNode,
		            childNodes = clickedListPicture.parentNode.childNodes,
		            numChildNodes = childNodes.length,
		            nodeIndex = 0,
		            index;

		        for (var i = 0; i < numChildNodes; i++) {
		            if(childNodes[i].nodeType !== 1) { 
		                continue; 
		            }

		            if(childNodes[i] === clickedListPicture) {
		                index = nodeIndex;
		                break;
		            }
		            nodeIndex++;
		        }



		        if(index >= 0) {
		            // open PhotoSwipe if valid index found
		            openPhotoSwipe( index, clickedGallery );
		        }
		        return false;
		    };

		    // parse picture index and gallery index from URL (#&pid=1&gid=2)
		    var photoswipeParseHash = function() {
		        var hash = window.location.hash.substring(1),
		        params = {};

		        if(hash.length < 5) {
		            return params;
		        }

		        var vars = hash.split('&');
		        for (var i = 0; i < vars.length; i++) {
		            if(!vars[i]) {
		                continue;
		            }
		            var pair = vars[i].split('=');  
		            if(pair.length < 2) {
		                continue;
		            }           
		            params[pair[0]] = pair[1];
		        }

		        if(params.gid) {
		            params.gid = parseInt(params.gid, 10);
		        }

		        return params;
		    };

		    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
		        var pswpElement = document.querySelectorAll('.pswp')[0],
		            gallery,
		            options,
		            items;

		        items = parseThumbnailElements(galleryElement);

		        // define options (if needed)
		        options = {

		            // define gallery index (for URL)
		            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

		            getThumbBoundsFn: function(index) {
		                // See Options -> getThumbBoundsFn section of documentation for more info
		                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
		                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
		                    rect = thumbnail.getBoundingClientRect(); 

		                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
		            }

		        };

		        // PhotoSwipe opened from URL
		        if(fromURL) {
		            if(options.galleryPIDs) {
		                // parse real index when custom PIDs are used 
		                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
		                for(var j = 0; j < items.length; j++) {
		                    if(items[j].pid == index) {
		                        options.index = j;
		                        break;
		                    }
		                }
		            } else {
		                // in URL indexes start from 1
		                options.index = parseInt(index, 10) - 1;
		            }
		        } else {
		            options.index = parseInt(index, 10);
		        }

		        // exit if index not found
		        if( isNaN(options.index) ) {
		            return;
		        }

		        if(disableAnimation) {
		            options.showAnimationDuration = 0;
		        }

		        // Pass data to PhotoSwipe and initialize it
		        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
		        gallery.init();
		    };

		    // loop through all gallery elements and bind events
		    var galleryElements = document.querySelectorAll( gallerySelector );

		    for(var i = 0, l = galleryElements.length; i < l; i++) {
		        galleryElements[i].setAttribute('data-pswp-uid', i+1);
		        galleryElements[i].onclick = onThumbnailsClick;
		    }

		    // Parse URL and open gallery if it contains #&pid=3&gid=1
		    var hashData = photoswipeParseHash();
		    if(hashData.pid && hashData.gid) {
		        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
		    }
		};

		// execute above function
		initPhotoSwipeFromDOM('.dyn-slider .pictures');
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>