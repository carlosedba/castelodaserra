<?php
namespace Rise;

use \PDO;
use \ORM;
use Rise\Model;

class Container extends \Slim\Container
{
	private $renderer;

	public function __construct(array $values = [])
	{
		parent::__construct(array('settings' => $values['config']['netcore']));

		$this->initializeDatabase();
		$this->setStore();
		$this->setRenderer();
		$this->setAliases();
	}

	public function initializeDatabase()
	{
		$settings = $this->get('settings');

		ORM::configure("{$settings['db']['driver']}:host={$settings['db']['host']};dbname={$settings['db']['database']}");
		ORM::configure('username', $settings['db']['username']);
		ORM::configure('password', $settings['db']['password']);
		ORM::configure('return_result_sets', true);
		ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		Model::$auto_prefix_models = 'Rise\\Models\\';
		Model::$short_table_names = true;
	}

	public function setStore()
	{
		$this->offsetSet('store', function () {
			return new Store();
		});
	}

	public function setRenderer()
	{
		$settings = $this->get('settings');
		$store = $this->get('store');

		$this->renderer = new Renderer($settings['renderer'], $store);
	}

	public function setAliases()
	{
		$this->offsetSet('internal', function () {
			return $this->renderer->internal();
		});

		$this->offsetSet('theme', function () {
			return $this->renderer->theme();
		});
	}
}
?>
