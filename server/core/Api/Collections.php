<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Collections
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$collections = Model::factory('Collection')->findArray();
		$json = json_encode($collections);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$collections = Model::factory('Collection')->where('id', $id)->findOne()->asArray();
		$json = json_encode($collections);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$collection = Model::factory('Collection')->create(array(
			'id' 			=> IdGenerator::uniqueId(8),
			'name' 			=> $data['name'],
			'description' 	=> $data['description'],
			'picture' 		=> $data['picture'],
		));

		if ($collection->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$collection = Model::factory('Collection')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($collection->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$collection = Model::factory('Collection')->where('id', $id)->findOne();

		if ($collection->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
