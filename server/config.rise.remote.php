<?php
return [
	'config' => [
		'production' => true,

		'netcore' => [
			'displayErrorDetails' => true, // set to false in production
			'addContentLengthHeader' => false, // Allow the web server to send the content-length header
			'determineRouteBeforeAppMiddleware' => false,

			// Renderer settings
			'renderer' => [
				'internal' => [
					'cache' => CACHE_PATH,
					'templates' => TEMPLATES_PATH,
				],

				'theme' => [
					'cache' => CACHE_PATH,
					'templates' => THEMES_PATH,
				]
			],

			'db' => [
				'driver' 		=> 'mysql',
				'host' 			=> 'castelodaserra.mysql.dbaas.com.br',
				'database' 		=> 'castelodaserra',
				'username'	 	=> 'castelodaserra',
				'password' 		=> 'advjuarez41',
				'charset'   	=> 'utf8',
				'collation' 	=> 'utf8_unicode_ci',
				'prefix'    	=> 'rise_',
			],
		],

		// Monolog settings
		'logger' => [
			'name' => 'rise',
			'path' => LOGS_PATH . '/rise.log',
			'level' => \Monolog\Logger::DEBUG,
		],
	],
];

